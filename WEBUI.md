# WebApp Game Controller with TouchControlsJS
HTML, CSS, and JavaScript mobile web application with a websocket connection to the LoCoRo webserver.py Python program.

The WebApp uses the touchcontrols.js library to render common game controller user interface elements.

## Installation
The webcontroller is install as part of the py-locoro project and does not have a separate installer.

## Components

#### Touch Controls Library

The touch controls are provided from the touchcontrolsjs library. This library supports the following user interface objects:

    controltype | Control
    --------|----------
    joystick | Joystick
    vslider | Vertical Slider
    hslider | Horizontal Slider
    round | Round Button
    square | Square Button
    up | Up Triangle Button
    left | Left Triangle Button
    down | Down Triangle Button
    right | Right Triangle Button

#### HTML Layout

Use HTML and CSS to position and size the area for each control. Assign each area a unique ID. The location of controls are best specified in HTML using CSS styles such as absolute or relative positioning. The size of controls is also specified in HTML using CSS styles for width and height.

```
<!-- place your elements however you like but insure they have a width and height -->                                                 
<div id="joystick01" style="position: absolute; left:    0px; bottom:  20px; width: 200px; height: 200px;"></div>                     
<div id="hslider01"  style="position: absolute; right:  20px; bottom: 100px; width: 180px; height:  40px;"></div>           
<div id="stopbutton" style="position: absolute; left:   90px; top:     90px; width:  50px; height:  50px;"></div>          
```

### JavaScript Mapping and Callback

Use JavaScript to assign a the rendering, and touch interaction to each control area defined in the HTML.
```
// ui = addTouchControl ("areaID", "controltype", {options});
// the areaID is a string representation of the HTML ID where the control will be displayed
// the controltype is a string name of the type of control (see the table above)
// the options are comma delimited and may be any combination of: uiColor as a hex string value, touchColor as a hex string color, controlCallback as a function name, autoCenter as a boolean, and roundRect as a boolean. At a minimum, the uiCallback should be specified, otherwise the control will have no action.

ui = addTouchControl ("stopbutton", "round", { uiCallback: my_callback, uiColor: "#f00000", touchColor: "#FF7777" });
```
The touchcontrols library will access the DOM to get the position and size of each control.

The controls have two color states - the uiColor specifies the normal color when the control is idle and the touch Color is the used when the control is active such as when a button is being pressed.

The Joystick and sliders support the autoCenter option. When this is `true` the control will return to its center position when not actively being touched.

The horizontal and vertical sliders support the roundRect option. When this is `false` the controls have a rectangular outline and a square slider. When this is `true` the control is rendered as a rounded rectangle and has a circle slider.

Each control's callback is triggered any time the control changes its state. A callback must support three parameters - a reference to the UI (this is the same UI object returned when the control was created with `addTouchControl()`), a deltaX value and a deltaY value. 

```
function my_callback (ui, deltaX, deltaY)
{
    // this is a button callback and we are only interested in the press event and not the release event
    if (deltaX) {
        // a non-zero value means the button is being pressed
        // do something
    }
}
```

For buttons, the delta values will both have the same numerical value and will be `1.0` when pressed and `0.0` when released.

For horizontal sliders, the deltaY will range from -1.0 to 1.0 and the deltaX will always be 0.0.

For vertical sliders, the deltaX will range from -1.0 to 1.0 and the deltaY will always be 0.0.

For a joystick, the deltaX and deltaY will range from -1.0 to 1.0.

