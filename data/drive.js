
var msg_el = null;
var err_el = null;

var ws = null;
var drive_sensitivity = 0.5;

function display_message (msg)
{
	if (!msg_el)
		msg_el = document.getElementById("messagearea");
	
	msg_el.innerHTML = msg;
	// console.log (msg);
}

function display_error (msg)
{
	if (!err_el)
		err_el = document.getElementById("errorarea");
	
	err_el.innerHTML = msg;
	// console.log (msg);
}

function drive_callback (ui, deltaX, deltaY)
{
	// touchcontrolsjs delatX & deltaY are -1.0..1.0
	// remember values so they may be mixed with other drive input such as when we are supporting strafe for mecanum drive

	// differential drive needs to convert the joystick X&Y to speeds for left and right wheels/tracks
	// joystick fore/aft is speed and left/right is angle

	// Differential Steering Joystick Algorithm
	// ========================================
	//   by Calvin Hass
	//   https://www.impulseadventure.com/elec/
	//
	// Converts a single dual-axis joystick into a differential
	// drive motor control, with support for both drive, turn
	// and pivot operations.
	//

	// OUTPUTS
	var nMotMixL, nMotMixR; // Motor (left/right)  mixed output (1.0 .. +1.0)

	// CONFIG
	// - fPivYLimt  : The threshold at which the pivot action starts
	//                This threshold is measured in units on the Y-axis
	//                away from the X-axis (Y=0). A greater value will assign
	//                more of the joystick's range to pivot actions.
	//                Allowable range: (0..+1.0)
	var fPivYLimit = 0.5;

	// TEMP VARIABLES
	var nMotPremixL;    // Motor (left/right)  premixed output (1.0 .. +1.0)
	var nPivSpeed;      // Pivot Speed  (1.0 .. +1.0)
	var fPivScale;      // Balance scale b/w drive and pivot (0 .. 1)

	// Calculate Drive Turn output due to Joystick X input
	if (deltaY < 0) {
		// Reverse
		nMotPremixL = (deltaX >= 0) ? 1.0 : (1.0 + deltaX);
		nMotPremixR = (deltaX >= 0) ? (1.0 - deltaX) : 1.0;
	} else {
		// Forward
		nMotPremixL = (deltaX >= 0) ? (1.0 - deltaX) : 1.0;
		nMotPremixR = (deltaX >= 0) ? 1.0 : (1.0 + deltaX);
	}

	// Scale Drive output due to Joystick Y input (throttle)
	nMotPremixL = nMotPremixL * deltaY;
	nMotPremixR = nMotPremixR * deltaY;

	// Now calculate pivot amount
	// - Strength of pivot (nPivSpeed) based on Joystick X input
	// - Blending of pivot vs drive (fPivScale) based on Joystick Y input
	nPivSpeed = deltaX;
	fPivScale = (Math.abs(deltaY) > fPivYLimit) ? 0.0 : (1.0 - Math.abs(deltaY) / fPivYLimit);

	// NOTE: for some reason (likely developer stupidity) the SCREAMZY need L & R swapped
	//       it may have to do with the fact that the devloper doesnt really have a clue about the pivot modifier
	// Calculate final mix of Drive and Pivot
	nMotMixR = (1.0 - fPivScale) * nMotPremixL + fPivScale * (-nPivSpeed);
	nMotMixL = (1.0 - fPivScale) * nMotPremixR + fPivScale * (nPivSpeed);

	var message = {
		'cmd': 'speed',
		'left': nMotMixL,
		'right': nMotMixR
	};
	ws.send(JSON.stringify(message));

	display_message("Speed X:" + nMotMixL.toFixed(3) + " Y:" + nMotMixR.toFixed(3));
	// display_message(ui.elementName + " " + left.toFixed(4) + " " + right.toFixed(4));
}


function drive_dance_callback (ui, deltaX, deltaY)
{
	if (deltaX) {
		// only call on button down event
		var message = {
			'cmd': 'dance'
		};
		ws.send(JSON.stringify(message));
	}
	// display_message (ui.elementName + " " + deltaX.toFixed(4) + " " + deltaY.toFixed(4));
}
