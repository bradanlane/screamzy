function winch_callback(ui, deltaX, deltaY) {
	// touchcontrolsjs delatX & deltaY are -1.0..1.0
	// remember values so they may be mixed with other drive input such as when we are supporting strafe for mecanum drive

	// a vertical slider uses deltaY
	// a horizontal slider uses deltaX

	// the winch is too sensitive (and dangerous) so we scale it with a ramp effect
	// also reverse the winch direction
	deltaX = -1.0 * (deltaX * 0.25);

	var message = {
		'cmd': 'power',
		'id': 8,
		'speed': deltaX
	};
	ws.send(JSON.stringify(message));

	display_message("Power:" + deltaX.toFixed(3));
	// display_message(ui.elementName + " " + left.toFixed(4) + " " + right.toFixed(4));
}


function my_init()
{
	// setup the touch control interface
	var ok = initTouchControls();
	
	if (!ok) {
		display_error ("no touch device found");
	} else {
		// render UI controls
		addTouchControl("drive_joystick", "joystick", {uiColor : '#FF2222', touchColor : '#CC2222', uiCallback : drive_callback, autoCenter : true });
		addTouchControl("winch_slider", "hslider", { uiColor: '#FFFF22', touchColor: '#CCCC22', uiCallback: winch_callback, autoCenter: true });

		ws = new WebSocket('ws://' + location.host + ':81'); //localhost
		
		ws.onopen = function () {
			display_message('Connected to ' + location.hostname);
		}
		ws.onerror = function(error) {
			display_error('Error connecting to ' + location.hostname + ': ' + error);
		}
		ws.onclose = function() {
			display_message('Connection to ' + location.hostname + ' closed.');
		}
		ws.onmessage = function(e) {
			obj = JSON.parse(e.data);
			if (obj.success)
				display_message(obj.message);
			else
				display_error(obj.message);
		}
	}
}
