function left_callback(ui, deltaX, deltaY) {
	// touchcontrolsjs delatX & deltaY are -1.0..1.0
	// remember values so they may be mixed with other drive input such as when we are supporting strafe for mecanum drive

	// a vertical slider uses deltaY
	// a horizontal slider uses deltaX


	var message = {
		'cmd': 'power',
		'id': 12,
		'speed': deltaY
	};
	ws.send(JSON.stringify(message));

	display_message("PowerL:" + deltaY.toFixed(3));
	// display_message(ui.elementName + " " + left.toFixed(4) + " " + right.toFixed(4));
}

function right_callback(ui, deltaX, deltaY) {
	// touchcontrolsjs delatX & deltaY are -1.0..1.0
	// remember values so they may be mixed with other drive input such as when we are supporting strafe for mecanum drive

	// a vertical slider uses deltaY
	// a horizontal slider uses deltaX


	var message = {
		'cmd': 'power',
		'id': 13,
		'speed': deltaY
	};
	ws.send(JSON.stringify(message));

	display_message("PowerR:" + deltaY.toFixed(3));
	// display_message(ui.elementName + " " + left.toFixed(4) + " " + right.toFixed(4));
}

function my_init()
{
	// setup the touch control interface
	var ok = initTouchControls();
	
	if (!ok) {
		display_error ("no touch device found");
	} else {
		// render UI controls
		addTouchControl("left_slider", "vslider", { uiColor: '#FFFF22', touchColor: '#CCCC22', uiCallback: left_callback, autoCenter: false });
		addTouchControl("right_slider", "vslider", { uiColor: '#FFFF22', touchColor: '#CCCC22', uiCallback: right_callback, autoCenter: false });


		ws = new WebSocket('ws://' + location.host + ':81'); //localhost
		
		ws.onopen = function () {
			display_message('Connected to ' + location.hostname);
		}
		ws.onerror = function(error) {
			display_error('Error connecting to ' + location.hostname + ': ' + error);
		}
		ws.onclose = function() {
			display_message('Connection to ' + location.hostname + ' closed.');
		}
		ws.onmessage = function(e) {
			obj = JSON.parse(e.data);
			if (obj.success)
				display_message(obj.message);
			else
				display_error(obj.message);
		}
	}
}
