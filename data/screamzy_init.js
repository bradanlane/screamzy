
function my_init()
{
	// setup the touch control interface
	var ok = initTouchControls();
	
	if (!ok) {
		display_error ("no touch device found");
	} else {
		// render UI controls
		addTouchControl("drive_joystick", "joystick", {uiColor : '#FF2222', touchColor : '#CC2222', uiCallback : drive_callback, autoCenter : true });


		ws = new WebSocket('ws://' + location.host + ':81'); //localhost
		
		ws.onopen = function () {
			display_message('Connected to ' + location.hostname);
		}
		ws.onerror = function(error) {
			display_error('Error connecting to ' + location.hostname + ': ' + error);
		}
		ws.onclose = function() {
			display_message('Connection to ' + location.hostname + ' closed.');
		}
		ws.onmessage = function(e) {
			obj = JSON.parse(e.data);
			if (obj.success)
				display_message(obj.message);
			else
				display_error(obj.message);
		}
	}
}
