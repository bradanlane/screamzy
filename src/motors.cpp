/* ***************************************************************************
* File:    motors.cpp
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### Motors API
Provides motor speed and direction using either the PCA9685 I2C PWM board or the TB6612FNG motor controller.

The non-PWM code is closely based off of: https://github.com/Nandopolis/TB6612FNG
and the only changes are to allow for externally controlled standby mode. (This saves one GPIO pin).

The I2C code uses the Adafruit library.
--- */

#include "motors.h"
#include "pins.h"
#include <Arduino.h>

/* ---
**Useful Notes:**
--- */

/* ---
This code is specific to the ESP8266 and requires int16 for the PWM values.
--- */

#ifdef MOTOR_I2C // the motors are either driven using an I2C PWM board or a TB66612FNG motor controller

#include <Adafruit_PWMServoDriver.h>
#include <Wire.h>

// using the default address 0x40
// static TwoWire i2c_device;
static Adafruit_PWMServoDriver PCA9685Board = Adafruit_PWMServoDriver(/* i2c_device, 0x40 */);

#define PWM_FREQUENCY 50
#define PWM_MIN 125
#define PWM_MAX 465
#define PWM_STOP 295 // STOP value should be mid point between MIN and MAX

// drive motor IDs
#define PWM_LMOTOR 12
#define PWM_RMOTOR 13
//#define PWM_WINCH 8	// the web interface drives the motor by sending the channel number

#elif MOTOR_TB6612FNG

//#define PWMRANGE 1023
#include "tb6612fng.inc"

static TB6612FNG motorLeft = TB6612FNG(PIN_PWMA, PIN_AIN1, PIN_AIN2);
static TB6612FNG motorRight = TB6612FNG(PIN_PWMB, PIN_BIN1, PIN_BIN2);

#else

#pragma GCC error "need to define motor controller - either MOTOR_I2C or MOTOR_TB6612FNG"

#endif // MOTOR_I2C or MOTOR_TB6612FNG



#ifdef MOTOR_I2C
uint16_t speed_to_pwm(double speed) {

	// TODO something wierd is going on at +1.0 so have a temporary hack to drop it to 0.99
	if (speed >= 1.0) speed = 0.99;

	uint16_t pwm_value = PWM_STOP;
	if (speed > 0)
		pwm_value = ((PWM_MAX - PWM_STOP) * speed) + PWM_STOP;
	else
		pwm_value = ((PWM_STOP - PWM_MIN) * speed) + PWM_STOP;

	// Serial.printf("speed:%+5.3f = %04d\n", speed, pwm_value);
	return pwm_value;
}
#endif

static double left_speed, right_speed;

/* ---
#### motorsInit()
Performs all necessary initialization. Must be called once before using any of the other motors functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool motorsInit() {
#ifdef MOTOR_I2C
	Wire.begin(5, 4); // SDA:SCL
	PCA9685Board.begin();
	// Analog servos and most brushed dc motor ESCs run at 50Hz or 60Hz updates
	PCA9685Board.setPWMFreq(PWM_FREQUENCY);
#endif
#ifdef MOTOR_TB6612FNG 
	motorLeft.Setup();
	motorRight.Setup();
#endif
	left_speed = right_speed = 0;
	return true;
}

/* ---
#### motorsFullStop()
Stop all motors.
 - return: **nothing**
--- */
void motorsFullStop() {
	//motorLeft.Brake();
	//motorRight.Brake();
#ifdef MOTOR_I2C
	PCA9685Board.setPWM(PWM_LMOTOR, 0, PWM_STOP);
	PCA9685Board.setPWM(PWM_RMOTOR, 0, PWM_STOP);
#endif

#ifdef MOTOR_TB6612FNG
	motorLeft.Coast();
	motorRight.Coast();
#endif

	left_speed = right_speed = 0;
}

/* ---
#### motorsPower()
Drive a single motor at a specificd power/speed. Only useful when there are non-drive motors
 - input:
   - mID **uint16_t** a zero based identification for a motor (with I2C this is 0 .. 15)
   - mSpeed **double** a value between -1 .. +1 for the motor
 - return: **nothing**
--- */
void motorsPower(uint16_t mID, double mSpeed) {
	// Serial.printf("motor[%02d]:%+5.3f\n", mID, mSpeed);
#ifdef MOTOR_I2C
	PCA9685Board.setPWM(mID, 0, speed_to_pwm(mSpeed));
#else
	Serial.println("WARNING: motorsPower() not implemented");
#endif
}

/* ---
#### motorsSpeed()
Drive each motor at a specificd speed - commonly refered to as diferential drive or tank drive mode.
 - input:
   - lSpeed **double** a value between -1 .. +1 for the left motor
   - rSpeed **double** a value between -1 .. +1 for the right motor
 - return: **nothing**
--- */
void motorsSpeed(double lSpeed, double rSpeed) {
	//Serial.printf("left:%+5.3f  right:%+5.3f\n", lSpeed, rSpeed);
#ifdef MOTOR_I2C
	// since the L/R motors are mounted opposed to each other, we need to invert one side
	PCA9685Board.setPWM(PWM_LMOTOR, 0, speed_to_pwm(lSpeed));
	PCA9685Board.setPWM(PWM_RMOTOR, 0, speed_to_pwm(-1.0 * rSpeed));
#endif

#ifdef MOTOR_TB6612FNG
	// the motor mixup is because of how they were soldered - stupid mistake
#ifdef SCREAMZY_V1
	// I likely wired the motors in such a way that this worked without the expected inverting of one motor
	motorLeft.Move(lSpeed * PWMRANGE);
	motorRight.Move(rSpeed * PWMRANGE);
#endif
#ifdef SCREAMZY_V3
//	motorLeft.Move(lSpeed * PWMRANGE);
//	motorRight.Move(rSpeed * PWMRANGE);
	motorLeft.Move((lSpeed)*PWMRANGE);
	motorRight.Move((-1.0 * rSpeed) * PWMRANGE);
#endif
#endif

	// save new speed settings.
	left_speed = lSpeed;
	right_speed = rSpeed;
}

/* ---
#### motorsDrive()
Drive at a requested speed while turning the requested amount. The actual speed and turn will be constrained.
 - input:
   - speed **double** a value between 0 .. +1
   - turn **double** a value between -1 .. +1 for the amount left (-) or right (+) to turn
   - zero **bool* `true` to inticate a zero radius turn is permitted, `false` will prevent a wheel from rotating backwards.
 - return: **nothing**

 NOTE: the differential steering algorithm is by Calvin Hass https://www.impulseadventure.com/elec/
--- */
void motorsDrive(double speed, double turn, bool zTurn) {
	// LIMIT  : The threshold at which the pivot action starts
	//          This threshold is measured in units on the Y-axis
	//          away from the X-axis (Y=0). A greater value will assign
	//          more of the joystick's range to pivot actions.
	//          Allowable range: (0..+1.0)
#define MOTOR_PIVOT_LIMIT 0.5
#define ABS(n) (((n) < 0) ? (-n) : (n))

	float nMotMixL, nMotMixR;		// Motor (left/right)  mixed output (1.0 .. +1.0)
	float nMotPremixL, nMotPremixR; // Motor (left/right)  premixed output (1.0 .. +1.0)
	float nPivSpeed;				// Pivot Speed  (1.0 .. +1.0)
	float fPivScale;				// Balance scale b/w drive and pivot (0 .. 1)


	// Calculate Drive Turn output due to Joystick X input
	if (speed >= 0) {
		// Forward
		nMotPremixL = (turn >= 0.0) ? (1.0 - turn) : 1.0;
		nMotPremixR = (turn >= 0.0) ? 1.0 : (1.0 + turn);
	} else {
		// Reverse
		nMotPremixL = (turn >= 0.0) ? 1.0 : (1.0 + turn);
		nMotPremixR = (turn >= 0.0) ? (1.0 - turn) : 1.0;
	}

	// Scale Drive output due to Joystick Y input (throttle)
	nMotPremixL = nMotPremixL * speed;
	nMotPremixR = nMotPremixR * speed;

	// Now calculate pivot amount
	// - Strength of pivot (nPivSpeed) based on Joystick X input
	// - Blending of pivot vs drive (fPivScale) based on Joystick Y input
	nPivSpeed = turn;
	fPivScale = (ABS(speed) > MOTOR_PIVOT_LIMIT) ? 0.0 : (1.0 - ABS(speed) / MOTOR_PIVOT_LIMIT);

	// Calculate final mix of Drive and Pivot
	nMotMixR = (1.0 - fPivScale) * nMotPremixL + fPivScale * (-nPivSpeed);
	nMotMixL = (1.0 - fPivScale) * nMotPremixR + fPivScale * (nPivSpeed);

	if (!zTurn) { // blend L/R to eliminate pivot
		// TODO
	}

	motorsSpeed((nMotMixL), (nMotMixR));
}

double motorsGetLeftSpeed() {
	return left_speed;
}

double motorsGetRightSpeed() {
	return right_speed;
}
