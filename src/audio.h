#ifndef __AUDIO_H_
#define __AUDIO_H_

#include <Arduino.h>

bool audioInit();
void audioUpdate();

double audioGetAmplitude();
double audioGetDeflection();

#endif
