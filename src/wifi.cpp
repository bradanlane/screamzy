/* ***************************************************************************
* Project: SCREAMZY Toy
* File:    wifi.cpp
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the GNU General Public License 3 (or, at your option. any later version)
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### WIFI API
Provides wifi or hotspot access and enables over-the-aor updates.
--- */

#include "wifi.h"
#include "credentials.h"
#include "leds.h"

#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266mDNS.h>
#include <functional>

ESP8266WiFiMulti wifiMulti;		   	// Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'
const char *mdnsRoot = "screamzy"; 	// Domain name for the mDNS responder
static char mdnsName[32];			// make it a composite of 'screamzy' and the last hex value from the MAC address

static bool g_wifi_connected = false;
static bool g_wifi_is_hotspot = false;
static WiFiMode_t g_wifi_mode = WIFI_AP;

static bool startNetwork() { // Start a Wi-Fi access point, and try to connect to some given access points. Then wait for either an AP or STA connection
	// int channel = random(12) + 1;
	uint8_t addr[6];
	char hotspot_name[32];
	bool state = true;
	int i = 0;

	g_wifi_connected = false;
	g_wifi_is_hotspot = false;

	WiFi.softAPmacAddress(addr);
	sprintf(hotspot_name, "%s_%02x%02x%02x", mdnsRoot, addr[3], addr[4], addr[5]);
	sprintf(mdnsName, "%s%02x", mdnsRoot, addr[5]);

	// add as many networks as you want ...
	wifiMulti.addAP(g_networkSSID, g_networkPassword); // add Wi-Fi networks you want to connect to when available
#ifdef CREDS2
	wifiMulti.addAP(g_networkSSID2, g_networkPassword2);
#endif
#ifdef CREDS3
	wifiMulti.addAP(g_networkSSID3, g_networkPassword3);
#endif
#ifdef CREDS4
	wifiMulti.addAP(g_networkSSID4, g_networkPassword4);
#endif

	Serial.print("Connecting ... ");
	while (wifiMulti.run() != WL_CONNECTED && WiFi.softAPgetStationNum() < 1) { // Wait for a Wi-Fi connection
		Serial.print(".");

		// we will attempt for 10 seconds
		delay(500);
		if (i++ > 20) {
			state = false;
			break;
		}
	}
	Serial.println("\n");

	if (state) {
		Serial.printf("SCREAMZY connected to %s as %s\n", WiFi.SSID().c_str(), wifiAddress());
		g_wifi_connected = true;
	} else if (WiFi.softAP(hotspot_name /*, NULL, channel */)) { // Start the access point with no password
		Serial.printf("established a hotspot as %s\n", hotspot_name);
		g_wifi_is_hotspot = true;
		g_wifi_connected = true;
	} else {
		Serial.printf("failed to connect to an access point and failed to establish a hotspot as %s\n", hotspot_name);
	}

	g_wifi_mode = WiFi.getMode();

	return g_wifi_connected;
}

static void startMDNS() { // Start the mDNS responder
	MDNS.begin(mdnsName); // start the multicast domain name server
	Serial.printf("mDNS responder started: http://%s.local\n", mdnsName);
}

/* ---
#### wifiInit()
Performs all necessary initialization. Must be called once before using any of the other wifi functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool wifiInit() {
	g_wifi_connected = startNetwork(); // Start a Wi-Fi access point, and try to connect to some given access points. Then wait for either an AP or STA connection
	startMDNS();				// Start the mDNS responder
	return g_wifi_connected;
}

/* ---
#### wifiLoop()
Give the WiFi services an opportunity to respond to any necessary actions
--- */
void wifiLoop() {
	// TODO currently nothing to do. consider folding OAT into wifi
}

/* ---
#### wifiOff()
Turn the WiFi off; it saves the current state
--- */
void wifiOff() {
	WiFiMode_t mode = WiFi.getMode();
	if (mode != WIFI_OFF)
		g_wifi_mode = mode;
	WiFi.mode(WIFI_OFF);
}

/* ---
#### wifiOn()
Turn the WiFi off; it saves the current state
--- */
void wifiOn() {
	if (g_wifi_mode != WIFI_OFF)
		WiFi.mode(g_wifi_mode);
}


/* ---
#### wifiIsConnected()
get the current availability of wifi connectivity - either via an existing access point or as a hotspot
 - return: **bool** `true` when wifi is available and `false` when not
--- */
	bool wifiIsConnected() {
		return g_wifi_connected;
}

/* ---
#### wifiConnectionLost()
test is we still have wifi connectivity; if we never had wifi we can't lose somethign we never had
 - return: **bool** `true` if we lost a prior wifi connection and `false` if we are at the same state as before
--- */
bool wifiConnectionLost() {
	// we were connected but are no longer connected
	return (g_wifi_connected && (!g_wifi_is_hotspot) && (WiFi.status() != WL_CONNECTED));
}

/* ---
#### wifiIsHotspot()
indicate if teh wifi connection is a hotspot or connected to an access point
 - return: **bool** `true` when wifi is a hotspote and `false` when it is an access point
--- */
bool wifiIsHotspot() {
	return g_wifi_is_hotspot;
}

/* ---
#### wifiAddress()
a conveniece function to get the WiFi address as character string
 - return: **char* ** static internal buffer of the wifi address in the form nnn.nnn.nnn.nnn
--- */
char *wifiAddress() {
#define IPADDRSIZE 16
	static char buffer[IPADDRSIZE + 1];
	IPAddress ip = WiFi.localIP();
	snprintf(buffer, IPADDRSIZE, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
	return buffer;
}

/* ---
#### wifiAddressEnding()
a conveniece function to get the last value in the WiFi address
 - return: **uint8_t** last number in wifi address eg xxx.xxx.xxx.NNN
--- */
uint8_t wifiAddressEnding() {
	IPAddress ip = WiFi.localIP();
	return ip[3];
}

#define OTA_PORT 8266 // its the default :-)

/* ---
#### otaInit()
Performs all necessary initialization. Must be called once before using any of the other OAT functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool otaInit() {
	if (g_wifi_connected) {
		ArduinoOTA.setPort(OTA_PORT);
		ArduinoOTA.setHostname(g_otaHostname);
		ArduinoOTA.setPassword(g_otaPassword);

		ArduinoOTA.onStart([]() {
			String type;
			if (ArduinoOTA.getCommand() == U_FLASH) {
				type = "sketch";
			} else { // U_SPIFFS
				type = "filesystem";
			}

			// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
			Serial.println("Start OTA update of " + type);
		});

		ArduinoOTA.onEnd([]() {
			Serial.println("\nEnd OTA update");
		});

		ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
			static uint8_t toggle = 0;

			// blick the onboard LED
			digitalWrite(2, toggle);

			// since we know we have RGB LEDs we can wig-wag the progress
			uint8_t color;

			color = 31 * toggle;
			ledsSetPixelRGB(0, 0, color, 0);
			color = 31 * ((toggle + 1) % 2);
			ledsSetPixelRGB(1, 0, color, 0);
			ledsShow();

			toggle = (toggle + 1) % 2;
			Serial.printf("Progress of OTA update: %u%%\r", (progress / (total / 100)));
		});

		ArduinoOTA.onError([](ota_error_t error) {
			Serial.printf("OTA update error[%u]: ", error);
			if (error == OTA_AUTH_ERROR) {
				Serial.println("Auth Failed");
			} else if (error == OTA_BEGIN_ERROR) {
				Serial.println("Begin Failed");
			} else if (error == OTA_CONNECT_ERROR) {
				Serial.println("Connect Failed");
			} else if (error == OTA_RECEIVE_ERROR) {
				Serial.println("Receive Failed");
			} else if (error == OTA_END_ERROR) {
				Serial.println("End Failed");
			}
		});
		ArduinoOTA.begin();
		return true;
	}
	return false;
}

/* ---
#### otaLoop()
Give the OTA an opportunity to respond to over-the-air update requests
--- */
void otaLoop() {
	if (g_wifi_connected) {
#if 0
		static bool _allow_ota = true;
 		if (_allow_ota) {
			// only allow OTA during the first 30 seconds; this prevents loop congestion which messes up Alexa access
			if (millis() > 30000)
				_allow_ota = false;
		}
#endif

		static unsigned long _test_cycle = 0;

		// we allow testing for OTA periodically
		if (millis() > _test_cycle) {
			ArduinoOTA.handle();
			_test_cycle += 2000; // 2 seconds from now
		}
	}
}
