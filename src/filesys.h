#ifndef __FILE_SYSTEM_H
#define __FILE_SYSTEM_H

#include <FS.h>

bool filesysInit();
bool filesysLoop();

bool filesysExists(String name);
File filesysOpen(String name);
void filesysClose(File handle);

bool filesysSaveStart(String name);
bool filesysSaveWrite(uint8_t* buf, size_t size);
bool filesysSaveFinish();

#endif
