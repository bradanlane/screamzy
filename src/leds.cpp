/* ***************************************************************************
* Project: SCREAMZY Toy
* File:    leds.cpp
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the GNU General Public License 3 (or, at your option. any later version)
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### LEDS API
Control WS2812 (or SK6812) RGB LEDs

##### Usage:
If you are using an LED matrix, edit the following definitions:
 - `#define LEDS_MATRIX_WIDTH *<value>*`
 - `#define LEDS_MATRIX_HEIGHT *<value>*`

The matrix code assumes the matrix is wired top to bottom and left to right with serpentine wiring
*(the bottom of row 1 is wired to teh bottom of row 2, teh top of row 2 is wired to teh top of row 3, etc.)*

If you are using a string of LEDs, edit the following definition:
 - `#define LEDS_COUNT  *<value>*`

**Notes:**
 - This code currently assumes APA102 type LEDs. To use another type, edit `#define LEDS_TYPE *<value>*`.
 - This code requires `#define PIN_LED_DATA *<value>*` and optionally `#define PIN_LED_CLOCK *<value>*` (if a 2-wire LED type is used)
 - *The SCREAMZY runs the LEDs off of the 3.3V source which may be low for some LEDs and result in erratic behavior*

--- */

#define LEDS_TYPE WS2812B

// define COUNT *or* define MATRIX  !!! DO NOT DEFINE BOTH
#define LEDS_COUNT 2

#define LEDS_MATRIX_WIDTH 1
#define LEDS_MATRIX_HEIGHT 1

#ifndef LEDS_COUNT
#define LEDS_COUNT ((LEDS_MATRIX_WIDTH) * (LEDS_MATRIX_HEIGHT))
#endif

// #define FASTLED_INTERUPT_RETRY_COUNT 0
#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>

#include "leds.h"
#include "pins.h"

#define BRIGHTNESS 63 // reduce total brightness to conserve power

static CRGB g_leds[LEDS_COUNT];

// tetris pieces are numbered 0 and 1..7 yellow, cyan, blue, orange, purple, green, red
#define MAX_MAP_INDEX 8
static CRGB colorMap[MAX_MAP_INDEX] = {CRGB::Black, CRGB::Gold, CRGB::Cyan, CRGB::DarkBlue, CRGB::OrangeRed, CRGB::DarkMagenta, CRGB::DarkGreen, CRGB::DarkRed};
static bool _leds_inited = false;

/* This function will return the right 'led index number' for
 * a given set of X and Y coordinates on your matrix.
 *
 * the matrix runs top-bottom and left-right with serpentine wiring. 
 * on a 16x16 matrix the following numbering would occur
 *  - eg the first row first led is #0
 *  - eg the second row first led is #31
 *  - eg the third row first led is #32
 *  - ...
 * WARNING: IT DOES NOT CHECK THE COORDINATE BOUNDARIES.
 */

#ifdef LEDS_MATRIX_WIDTH
static uint16_t matrix_index(uint8_t x, uint8_t y) {
	uint16_t i;

	if (x & 0x01) {
		// Odd columns run backwards
		i = (x * LEDS_MATRIX_HEIGHT) + ((LEDS_MATRIX_HEIGHT - 1) - y);
	} else {
		// Even rows run forwards
		i = (x * LEDS_MATRIX_HEIGHT) + y;
	}
	return i;
}
#endif

/* ---
#### ledsInit()
Performs all necessary initialization. Must be called once before using any of the other LEDS functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool ledsInit() {
	// FastLED.addLeds<LEDS_TYPE, PIN_LED_DATA, PIN_LED_CLOCK, BGR, DATA_RATE_MHZ(10)>(g_leds, LEDS_COUNT);
#ifdef PIN_LED_CLOCK
	FastLED.addLeds<LEDS_TYPE, PIN_LED_DATA, PIN_LED_CLOCK, BGR, DATA_RATE_MHZ(10)>(g_leds, LEDS_COUNT);
#else
#if (LEDS_TYPE == NEOPIXEL)
	FastLED.addLeds<LEDS_TYPE, PIN_LED_DATA>(g_leds, LEDS_COUNT);
#else
	FastLED.addLeds<LEDS_TYPE, PIN_LED_DATA, RGB, DATA_RATE_MHZ(10)>(g_leds, LEDS_COUNT);
#endif
#endif

	FastLED.setBrightness(BRIGHTNESS);
	delay(100); // needed to give FastLED time to get ready? not sure

	_leds_inited = true;
	ledsClear();
	ledsShow();
	//_leds_inited = false;	// disable FastLED to noise interferance on ADC
	return _leds_inited;
}

/* ---
#### ledsClear()
Clears all LEDs by setting their value to black.
 - return: **bool** `true` on success and `false` on failure
--- */
bool ledsClear() {
	for (int i = 0; i < LEDS_COUNT; i++)
		g_leds[i] = CRGB::Black;
	return true;
}

/* ---
#### ledsShow()
Refreshes the display of the LEDs with their current values. Call this function after changes to the LED values (including `ledsClear()`) to see the results.
 - return: **bool** `true` on success and `false` on failure
--- */
void ledsShow() {
	if (_leds_inited)
		FastLED.show();

	// Serial.println("");
}

/* ---
#### ledsSetPixelRGB()
Set the value of an LED within an XY matrix to an RGB value.
 - input:
    - pos **uint16_t** a zero based value indicating the index of the LED within the strip
	- red **uint8_t** the red component of the RGB value for the LED
	- green **uint8_t** the green component of the RGB value for the LED
	- blue **uint8_t** the blue component of the RGB value for the LED
 - return: **bool** `true` on success and `false` on failure
--- */
bool ledsSetPixelRGB(uint16_t led, uint8_t r, uint8_t g, uint8_t b) {
	if (led < LEDS_COUNT) {
		g_leds[led] = CRGB(r, g, b); // need to figure out my mapping error
		g_leds[led].nscale8(BRIGHTNESS);
		// Serial.printf("[%04d] (%3d, %3d, %3d)    ", led, r, g, b);
	} else {
		// Serial.printf("[%04d] (inv)\n", led);
	}
	return true;
}

/* ---
#### ledsSetAllRGB()
Set the value of every LED within to an RGB value. and update display
 - input:
	- red **uint8_t** the red component of the RGB value for the LED
	- green **uint8_t** the green component of the RGB value for the LED
	- blue **uint8_t** the blue component of the RGB value for the LED
 - return: **bool** `true` on success and `false` on failure
--- */
bool ledsSetAllRGB(uint8_t r, uint8_t g, uint8_t b) {
	for (int i = 0; i < LEDS_COUNT; i++)
		ledsSetPixelRGB(i, r, g, b);
	ledsShow();
	return true;
}

/* ---
#### ledsSetPixelIndex()
Set the value of an LED within an XY matrix to an RGB value.
 - input:
    - pos **uint16_t** a zero based value indicating the index of the LED within the strip
	- color **uint8_t** the index into the zero based list of gaming colors: Black, Gold, Cyan, DarkBlue, OrangeRed, DarkMagenta, DarkGreen, DarkRed
 - return: **bool** `true` on success and `false` on failure
--- */
bool ledsSetPixelIndex(uint16_t led, uint8_t i) {
	if ((led < LEDS_COUNT) && (i < MAX_MAP_INDEX)) {
		g_leds[led] = colorMap[i];
		g_leds[led].nscale8(BRIGHTNESS);
		// Serial.printf("[%04d] (%3d)\n", led, i);
	}
	else {
		// Serial.printf("[%04d] (inv)\n", led);
	}
	
	return true;
}

#ifdef LEDS_MATRIX_WIDTH
/* ---
#### ledsMatrixSetPixelRGB()
Set the value of an LED within an XY matrix to an RGB value.
 - input:
    - row **uint8_t** a zero based value indicating the X coordinate of the LED within the matrix
    - col **uint8_t** a zero based value indicating the Y coordinate of the LED within the matrix
	- red **uint8_t** the red component of the RGB value for the LED
	- green **uint8_t** the green component of the RGB value for the LED
	- blue **uint8_t** the blue component of the RGB value for the LED
 - return: **bool** `true` on success and `false` on failure
--- */
bool ledsMatrixSetPixelRGB(uint8_t x, uint8_t y, uint8_t r, uint8_t g, uint8_t b) {
	return ledsSetPixelRGB(matrix_index(x, y), r, g, b);
}
/* ---
#### ledsMatrixSetPixelIndex()
Set the value of an LED within an XY matrix to an RGB value.
 - input:
    - row **uint8_t** a zero based value indicating the X coordinate of the LED within the matrix
    - col **uint8_t** a zero based value indicating the Y coordinate of the LED within the matrix
	- color **uint8_t** the index into the zero based list of gaming colors: Black, Gold, Cyan, DarkBlue, OrangeRed, DarkMagenta, DarkGreen, DarkRed
 - return: **bool** `true` on success and `false` on failure
--- */
bool ledsMatrixSetPixelIndex(uint8_t x, uint8_t y, uint8_t i) {
	return ledsSetPixelIndex(matrix_index(x, y), i);
}
#endif
