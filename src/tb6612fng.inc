// this code is included in mortors.cpp based on a the declaration of a definition

class TB6612FNG {
  private:
	uint8_t pwmPin;  /**< the pwm pin number */
	uint8_t in1Pin;  /**< the first direction pin number */
	uint8_t in2Pin;  /**< the second direction pin number */
	uint8_t stbyPin; /**< the standby pin number */
	boolean inited;
	boolean dir; /**< a boolean to change direction logic */

	/**
		 * @brief MoveRaw function
		 * 
		 * indicates the pwm and direction at which the load is drived
		 * 
		 * @param in1 the first direction pin value
		 * @param in2 the second direction pin value
		 * @param velocity the pwm value, from 0 to PWMRANGE (8 bits)
		 */
	void MoveRaw(bool in1, bool in2, uint16_t velocity) {
		// Serial.printf("v[%d,%d]:%+3d ", in1, in2, velocity);
		if (this->inited) {
			digitalWrite(this->in1Pin, in1);
			digitalWrite(this->in2Pin, in2);
			analogWrite(this->pwmPin, velocity);
		}
	}

  public:
	/**
		 * @brief Construct a new TB6612FNG object
		 * 
		 * @param pwmA the pwm pin
		 * @param in1A the first pin for direction
		 * @param in2A the second pin for direction
		 * @param stby the standby pin
		 * @param d the direction logic
		 */
	TB6612FNG(uint8_t pwmA, uint8_t in1A, uint8_t in2A, uint8_t stby = 99, bool d = 1) {
		this->pwmPin = pwmA;
		this->in1Pin = in1A;
		this->in2Pin = in2A;
		this->stbyPin = stby;
		this->dir = d;
		this->inited = true;
	}

	const bool standby = 0;		/**< constant for the standby mode */
	const bool normal = 1;		/**< constant for the normal mode */
	const uint8_t notused = 99; /**< contant to indicate the standby pin is externally controlled */

	/**
		 * @brief Setup function
		 * 
		 * sets OUTPUT pinMode for every pin
		 */
	void Setup() {
		if (this->inited) {
			if (this->stbyPin != notused)
				pinMode(this->stbyPin, OUTPUT);
			pinMode(this->in1Pin, OUTPUT);
			pinMode(this->in2Pin, OUTPUT);
			pinMode(this->pwmPin, OUTPUT);

			if (this->stbyPin != notused)
				digitalWrite(this->stbyPin, this->normal);
			digitalWrite(this->in1Pin, 0);
			digitalWrite(this->in2Pin, 0);
			analogWrite(this->pwmPin, 0);
		}
	}

	/**
		 * @brief Move function
		 * 
		 * sets the MoveRaw with the direction derived from the sign of velovity
		 * and the pwm as the absolute value of velocity 
		 * 
		 * @param velocity signed integer from -PWMRANGE to PWMRANGE
		 */
	void Move(int16_t velocity) {
		if (velocity > PWMRANGE) velocity = PWMRANGE;
		if (velocity < -PWMRANGE) velocity = -PWMRANGE;

		if (dir) {
			if (velocity < 0)
				this->MoveRaw(0, 1, (-velocity));
			else
				this->MoveRaw(1, 0, velocity);
		} else {
			if (velocity > 0)
				this->MoveRaw(0, 1, (-velocity));
			else
				this->MoveRaw(1, 0, velocity);
		}
	}

	/**
		 * @brief Brake function
		 * 
		 * sets in1 and in2 in order to do a fast brake
		 */
	void Brake() {
		if (this->inited) {
			digitalWrite(this->in1Pin, 1);
			digitalWrite(this->in2Pin, 1);
		}
	}
	void Coast() {
		if (this->inited) {
			digitalWrite(this->in1Pin, 1);
			digitalWrite(this->in2Pin, 0);
			analogWrite(this->pwmPin, 0);
		}
	}

	/**
		 * @brief Mode function
		 * 
		 * sets the stbyPin
		 * 
		 * @param mode 0 for standby, 1 for normal mode
		 */
	void Mode(bool mode) {
		if (inited) {
			if (this->stbyPin != notused)
				digitalWrite(this->stbyPin, mode);
		}
	}
};
