#ifndef __PINS_H
#define __PINS_H

/* ***************************************************************************
* Project: SCREAMZY Toy
* File:    main.cpp
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### ESP8266 GPIO pin assignments

The TB6612FNG motor controller requires 6 output pins (7 if you enable standby mode).
The MAX4466 adjustable gain microphone requries an analog input pin (The ESP8266 only has one).
The WS2812 RGB LEDs require one output pin.

*The ESP8266 has at most 12 usable pins but 2 (or 3) of those have existing useful functions 
which leave 9 (or 10) pins plus the single analog pin for most projects. 
(To get 10 usable pins, the RX pin is repurposed. Most projects do not receive serial data at runtime.)*
--- */

// For portability, we reference pins by their GPIO# and not the board specific identifiers.
#ifdef SCREAMZY_V1
#define AVAILABLE_0     0
#define AVAILABLE_1     1	 // RX is pulled high, can be used for a switch
#define PIN_LED_DATA    2 // The SK6812 LED strip data line
#define AVAILABLE_3     3	 // TX is pulled high, can be used for a switch but will cause garbage when debugging
#define PIN_AIN2        4
#define PIN_AIN1        5
#define DO_NOT_USE_6    6
#define DO_NOT_USE_7    7
#define DO_NOT_USE_8    8
#define DO_NOT_USE_9    9
#define AVAILABLE_10    10
#define DO_NOT_USE_11   11
#define PIN_BIN1        12
#define PIN_PWMB        13
#define PIN_BIN2        14
#define PIN_PWMA        15
#define PIN_SLEEP_WAKE  16

#elif SCREAMZY_V3

#define PIN_BIN1        0
#define AVAILABLE_1     1	 // RX is pulled high, can be used for a switch
#define PIN_BIN2        2
#define PIN_AIN1        3	 // TX is pulled high, can be used for a switch but will cause garbage when debugging
#define PIN_PWMA        4
#define PIN_AIN2        5
#define DO_NOT_USE_6    6
#define DO_NOT_USE_7    7
#define DO_NOT_USE_8    8
#define DO_NOT_USE_9    9
#define PIN_LED_DATA    10  // this was stupid since it means customizing the FastLED library - edit .pio/libdeps/nodemcuv2/FastLED_ID126/platforms/esp/8266/fastpin_esp8266.h line 55 to allow GPIO10
#define DO_NOT_USE_11   11

#define PIN_MEMS_CLK    13
#define PIN_MEMS_WS     14
#define PIN_MEMS_DATA   12

#define PIN_PWMB        15
#define PIN_SLEEP_WAKE  16

#else

#pragma GCC error "SCREAMZY_V? version not defined"

#endif  // SCREAMZY_V?

#endif
