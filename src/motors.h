#ifndef __MOTORS_H_
#define __MOTORS_H_

#include <Arduino.h>

bool motorsInit();
void motorsTest();
void motorsFullStop();
void motorsSpeed(double lSpeed, double rSpeed);
void motorsDrive(double speed, double direction, bool zTurn);

// CAUTION: this is only applicable with ESCs and probably only I2C driven ESCs
void motorsPower(uint16_t mID, double mSpeed);

double motorsGetLeftSpeed();
double motorsGetRightSpeed();

#endif
