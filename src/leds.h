/* ***************************************************************************
* Project: SCREAMZY Toy
* File:    leds.h
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the GNU General Public License 3 (or, at your option. any later version)
*
* ******************************************************************************/

#ifndef __LEDS_H
#define __LEDS_H

#include <Arduino.h>

bool ledsInit();
bool ledsClear();
void ledsShow();

bool ledsSetAllRGB(uint8_t r, uint8_t g, uint8_t b);

bool ledsSetPixelRGB(uint16_t n, uint8_t r, uint8_t g, uint8_t b);
bool ledsSetPixelIndex(uint16_t n, uint8_t i); // the index table is in the CPP code

bool ledsMatrixSetPixelRGB(uint8_t x, uint8_t y, uint8_t r, uint8_t g, uint8_t b);
bool ledsMatrixSetPixelIndex(uint8_t x, uint8_t y, uint8_t i); // the index table is in the CPP code

#endif

