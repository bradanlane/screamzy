/* ***************************************************************************
* Project: SCREAMZY Toy
* File:    audio.cpp
* Date:    2019.06.18
* Author:  Glen Salmon / Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the GNU General Public License 3 (or, at your option. any later version)
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### AUDIO API
Provides peak frequency and amplitude readings from an I2S MEMS microphone.

This code is based on work by Amanda Ghassaei. 
https://www.instructables.com/id/Arduino-Frequency-Detection/ 
Sept 2012
--- */

#include "audio.h"
#include <i2s.h>

// local variables for motor speed and direction
// values are in the range of [0.0 .. 1.0] and [-1.0 .. 1.0] respectively
static float audio_volume, audio_frequency_offset; //

/*
The `SAMPLING_FREQUENCY` is the maximum input frequency and must be less that 10K to deal with the ADC conversion time.
The `SAMPLING_RATE` is the integer value of 1,000,000 microseconds divided by the `SAMPLING_FREQUENCY`
*/

#define SAMPLING_DURATION 100000 // 0.1 seconds as the number of microseconds to perform sampling
#define SAMPLING_FREQUENCY 11025 // Hz
#define SAMPLING_RATE 90		 // integer aproximation of microseconds per sample to achieve sampling frequency

// Only frequencies **in Hz** within a portion of the vocal range are of interest
#define FREQUENCY_MAX 600
#define FREQUENCY_MIN 200
#define FREQUENCY_MID ((FREQUENCY_MAX + FREQUENCY_MIN) / 2)
#define FREQUENCY_RNG (FREQUENCY_MAX - FREQUENCY_MIN)

#define AMPLITUDE_THRESHOLD 75 //raise if you have a very noisy signal

#define SIGNAL_MAX 1000		// the value we consider MAX
#define SIGNAL_MIDPOINT 0 	// midpoint of signed 16 bit integer

/*
The adc.cpp file provides the interface between the raw analog audio input and
the extraction of peak frequency and amplitude values.
*/

/* use a rolling average to smooth out speed and direction changes */

#define SMOOTHING_SIZE 5
static int16_t smoothing_index;
static uint16_t smoothing_amplitude[SMOOTHING_SIZE];
static uint16_t smoothing_period[SMOOTHING_SIZE];

static void addDataToSmoothingTable(uint16_t amplitude, uint16_t period) {
	smoothing_amplitude[smoothing_index] = amplitude;
	smoothing_period[smoothing_index] = period;
	smoothing_index = (smoothing_index + 1) % SMOOTHING_SIZE;
}
static double getSmoothedAmplitude() {
	double val = 0;
	for (int i = 0; i < SMOOTHING_SIZE; i++)
		val += smoothing_amplitude[i];
	return (val / SMOOTHING_SIZE);
}
static double getSmoothedPeriod() {
	double val = 0;
	for (int i = 0; i < SMOOTHING_SIZE; i++)
		val += smoothing_period[i];
	return (val / SMOOTHING_SIZE);
}

#define EVENT_SIZE 10		  //data storage variables
int event_times[EVENT_SIZE];  // storage for timing of events
int event_slopes[EVENT_SIZE]; // storage for slope of events
int16_t data_new = 0;
int16_t data_rev = 0;
unsigned int sample_time = 0;   //keeps time_counter and sends values to store in timer[] occasionally
unsigned int sample_period = 0; //storage for sample_period of wave
uint8_t sample_index = 0;		//current storage index
int slope_max = 0;				//used to calculate max slope as trigger point
uint16_t match_failure = 0;		//counts how many non-matches you've received to reset variables if it's been too long

//variables for decided whether you have a match
#define SLOPE_TOLLERANCE 125 //slope tolerance - allowed change between samples ... adjust this if you need
#define TIMER_TOLLERANCE 15 //timer tolerance - max number of SAMPLE_RATEs allowed before giving up ... adjust this if you need

//variables for amp detection
unsigned int amplitude_timer = 0;
uint16_t amplitude_max = 0;

static void reset_sampling() { //clea out some variables
	sample_index = 0;		   //reset index
	match_failure = 0;		   //reset match counter
	slope_max = 0;			   //reset slope
							   //Serial.println("");
}

static void init_sampling() { //clea out some variables
	reset_sampling();
	amplitude_max = 0;
	sample_period = 0;
}

static void sample_adc() {				//when new ADC value ready
	unsigned int times_accumulator = 0; //used to calculate sample_period
	int slope_new = 0;
	bool success = false; //storage for incoming slope data
	int16_t dummy;

	data_rev = data_new; //store previous value
	data_new = 0;
	i2s_read_sample(&data_new, &dummy, true);
	//Serial.printf(" %04d ", data_new);

	if (data_rev < SIGNAL_MIDPOINT && data_new >= SIGNAL_MIDPOINT) { //if increasing and crossing midpoint
		slope_new = data_new - data_rev;							 //calculate slope
		if (abs(slope_new - slope_max) < SLOPE_TOLLERANCE) {		 //if slope has diminished - eg at the top of the wave
			//record new data and reset time
			event_slopes[sample_index] = slope_new;
			event_times[sample_index] = sample_time;
			sample_time = 0;
			if (sample_index == 0) {
				// the max slope was just just reset
				// we've started a new iteration
				match_failure = 0;
				sample_index++; //increment index
			} else if ((abs(event_times[0] - event_times[sample_index]) < TIMER_TOLLERANCE) && (abs(event_slopes[0] - slope_new) < SLOPE_TOLLERANCE)) {
				// the time difference between the first sample and the current sample is less than the max allowed
				// and the slope difference between first slope and the current slope is less that the threshold needed
				// aka our slope is flattening out
				//sum up the event times values (aka the total number of SAMPLE_RATEs which have passed)
				times_accumulator = 0;
				for (byte i = 0; i < sample_index; i++) {
					times_accumulator += event_times[i];
				}
				sample_period = times_accumulator; //set sample_period as the total number of SAMPLE_RATEs which have passed

				//reset new zero index values to compare with
				event_times[0] = event_times[sample_index];
				event_slopes[0] = event_slopes[sample_index];
				sample_index = 1; //set index to 1
				match_failure = 0;
				//Serial.println("event slope calculated");
				success = true; // we have valide data
			} else {
				//Serial.println("no slope match detected");
				// we are crossing midpoint but did not find a match
				sample_index++; //increment index
				if (sample_index > (EVENT_SIZE - 1)) {
					reset_sampling();
				}
			}
		} else if (slope_new > slope_max) {
			// the new slope is larger than our previously recoreded max slope
			// update_status(5);
			slope_max = slope_new;
			sample_time = 0;  //reset clock
			sample_index = 0; //reset index
			match_failure = 0;
		} else {
			//Serial.println("insufficient sampling slope");
			// the detected slope is not steep enough
			match_failure++; //increment no match counter
			if (match_failure > (EVENT_SIZE - 1)) {
				//Serial.println("repeated sampling slope error");
				reset_sampling();
			}
		}
	}

	sample_time++; //increment the counter with represents time as increments of SAMPLING_RATE

	amplitude_timer++; //increment amplitude timer
	if (success) {
		if (abs(SIGNAL_MIDPOINT - data_new) > amplitude_max) {
			amplitude_max = abs(SIGNAL_MIDPOINT - data_new);
		}
	}
}

/*
##### fetchAudioData()
a local function which reads raw audio samples
The function read data at a rate based on the SAMPLING_FREQUENCY constant.

- return: *nothing*
*/
static void fetchAudioData() {
	unsigned long next_time;
	unsigned long last_time;
	//unsigned long performance_timer = micros();
	unsigned int samples = 0;

	init_sampling();

	last_time = micros();

	next_time = last_time + SAMPLING_RATE;
	last_time += SAMPLING_DURATION + (SAMPLING_RATE / 2);

	do {
		while (micros() < next_time)
			yield(); // hurry up and wait
		sample_adc();
		next_time += SAMPLING_RATE;
		samples++;
	} while (next_time < last_time);

	//Serial.println("");
	//Serial.printf(" amplitude: %6d, period: %6d   ", amplitude_max, sample_period);

	// if we didnt get a valid frequency the default to center
	if ((sample_period > (SAMPLING_FREQUENCY / FREQUENCY_MIN)) ||
		(sample_period < (SAMPLING_FREQUENCY / FREQUENCY_MAX))) {
		sample_period = (SAMPLING_FREQUENCY / FREQUENCY_MID);
	}

	// if we didnt get enough amplitude then start slowing down
	if (amplitude_max < AMPLITUDE_THRESHOLD) {
		amplitude_max = getSmoothedAmplitude() / 2;
	}

	addDataToSmoothingTable(amplitude_max, sample_period);
	//performance_timer = micros() - performance_timer;
}

/* ---
#### adcInit()
Performs all necessary initialization. Must be called once before using any of the other ADC functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool audioInit() {

	i2s_rxtx_begin(true, false); // Enable I2S receive not transmit
	i2s_set_rate(SAMPLING_FREQUENCY);

	audio_volume = 0;
	audio_frequency_offset = 0;
	// initialze the smoothing table to neutral
	for (int i = 0; i < SMOOTHING_SIZE; i++)
		addDataToSmoothingTable(0, (SAMPLING_FREQUENCY / FREQUENCY_MID));

	return true;
}

/* ---
#### audioUpdate()
Fetches new audio samples and updates the amplitude and deflection data values.

Call this function to update values fetched with `adcGetAmplitude()` and `adcGetDeflection()`.
 - return: **nothing**
--- */
void audioUpdate() {
	float frequency; //storage for frequency calculations

	fetchAudioData(); // perform sampling of audio

	audio_volume = 0;
	audio_frequency_offset = 0;

	if (getSmoothedAmplitude() > AMPLITUDE_THRESHOLD) {
		frequency = SAMPLING_FREQUENCY / getSmoothedPeriod(); //calculate frequency timer rate/sample_period
		//Serial.printf("  %4.0f lv  %5.0f Hz    ", getSmoothedAmplitude(), frequency);
		audio_frequency_offset = (((frequency - FREQUENCY_MID)) / FREQUENCY_RNG) * 2.0;
		// scale the direction range of +/- DEFLECTION from MIDDLE
		audio_volume = getSmoothedAmplitude() / SIGNAL_MAX;
		if (audio_volume > 1.0)
			audio_volume = 1.0;
	} else {
		//Serial.printf("  %4.0f lv  %5.0f Hz    ", getSmoothedAmplitude(), (((double)SAMPLING_FREQUENCY) / getSmoothedPeriod()));
	}
	//Serial.println("");
}

/* ---
#### audioGetAmplitude()
Get the most recently updated peak audio amplitude relative to the internally configured volume range.
 - return: **double** a value between 0 .. +1 representing the relative speed from stop to full forward
--- */
double audioGetAmplitude() {
	return audio_volume;
}

/* ---
#### audioGetDeflection()
Get the most recently updated peak audio frequency relative to the internally configured frequency range.
 - return: **double** a value between -1 .. +1 representing the relative direction left to right
--- */
double audioGetDeflection() {
	return audio_frequency_offset;
}
