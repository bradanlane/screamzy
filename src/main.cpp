/* ***************************************************************************
* Project: SCREAMZY Toy
* File:    main.cpp
* Date:    2018.02.16
* Author:  Glen Salmon / Bradan Lane Studio
* ******************************************************************************/

/* ---
# SCREAMZY - the toy kids love and parents hate !

A toy which responds to volume and pitch to control speed and direction.
The louder you scream, the faster it goes.
A high pitch scream will go right and low pitch scream will go left.

Just imagine the all the fun you will have with a room full of kids!

In addition to sound control there is a mobile web interface - great for head to head racing competion.
It uses the browser on a smartphone or tablet to give a simple multi-touch interface.

Each toy will attempt to connect to the local WiFi and if it is unable, then it will
create its own unique hotspot. Multiple units can operate in the same room easily.

![SCREAMZY robot toy](https://gitlab.com/bradanlane/screamzy/raw/master/screamzy.png)

--------------------------------------------------------------------------
--- */

#include "audio.h"

#include "filesys.h"
#include "leds.h"
#include "motors.h"
#include "pins.h"
#include "web.h"
#include "wifi.h"

#include <Arduino.h>

#define LED_ON LOW
#define LED_OFF HIGH



static void update_leds() {
	// the WS2812B strip is making WAY too much noise on the line and the microphone is flooded
	double lspeed, rspeed;

	lspeed = motorsGetLeftSpeed();
	rspeed = motorsGetRightSpeed();

	// driving forward means L/R motors are actually rotating in oposite directions
	// the robot actually drives backward eg negative is forward and positive is reverse
	if (lspeed > 0.01)
		ledsSetPixelRGB(0, lspeed * 255, 0, 0);
	else if (lspeed < -0.01)
		ledsSetPixelRGB(0, 0, lspeed * 255, 0);
	else {
		if (wifiIsHotspot())
			ledsSetPixelRGB(0, 127, 127, 0);	// yellow on idle for hotspot
		else
			ledsSetPixelRGB(0, 0, 0, 127);	// blue on idle for access point
	}

	if (rspeed > 0.01)
		ledsSetPixelRGB(1, rspeed * 255, 0, 0);
	else if (rspeed < -0.01)
		ledsSetPixelRGB(1, 0, rspeed * 255, 0);
	else {
		if (wifiIsHotspot())
			ledsSetPixelRGB(1, 127, 127, 0); // yellow on idle for hotspot
		else
			ledsSetPixelRGB(1, 0, 0, 127); // blue on idle for access point
	}
	ledsShow();
}

static void update_motors_from_audio_input() {
	audioUpdate();
#if 1
	//Serial.printf("Speed: %5.3f  Direction: %+5.3f  ", audioGetAmplitude(), audioGetDeflection());
	motorsDrive(audioGetAmplitude(), audioGetDeflection(), false); // last parameter enables z-radius turning
#else
	//Serial.println("");
#endif
}

void setup() {
	Serial.begin(115200);
	while (!Serial)
		; // wait for serial attach

	Serial.println();
	Serial.println("Initializing...");
	Serial.flush();

	delay(100);
	
	ledsInit();

	ledsSetAllRGB(63, 63, 0); // yellow to indicate we are attempting to setup the wifi

	if (!wifiInit()) {
		ledsSetAllRGB(63, 0, 0); // red to indicate we have no wifi

		// we failed to get wifi so we slow blink the onboard LED 3 times
		for (int i = 0; i < 3; i++) {
			digitalWrite(2, LED_ON);  // turn the LED on
			delay(1000);			  // wait for a second
			digitalWrite(2, LED_OFF); // turn the LED off
			delay(500);				  // wait for a half second
		}
	} else {
		// anything we may want on SUCCESS
		ledsSetAllRGB(0, 0, 63); // blue to indicate we have wifi
	}

	otaInit();
	filesysInit();
	webInit();

	motorsInit();
	// TODO: fun little dance with motor commands
	motorsFullStop();

	audioInit();
	//wifiOff();	// temporarily disable WiFi whkle we develop I2S audio code ... and then hope to be able to re-enable WiFi

	Serial.println("Running...");
}

void loop() {
	webLoop(); 					// motors are updates directly from web events

	if (!webSocketActive())		// is no web socket connection, attempt to update motors from audio
		update_motors_from_audio_input();

	filesysLoop();
	wifiLoop();
	otaLoop();

	update_leds();

#if 0	// need to debug this as it will often happening after 10 seconds (or perhaps a multiple of 10 seconds)
	// we test if we had wifi but no longer have wifi; the shortcut to a full cleanup is to restart
	if (wifiConnectionLost()) {
		Serial.println("Connectivity Lost...");
		// let's stop the motors
		motorsFullStop();
		// and signal a problem
		for (int i = 0; i < 6; i++) {
			Serial.println("red green");
			ledsSetPixelRGB(0, 127, 0, 0);
			ledsSetPixelRGB(1, 0, 127, 0);
			ledsShow();
			delay(500);
			Serial.println("green red");
			ledsSetPixelRGB(0, 0, 127, 0);
			ledsSetPixelRGB(1, 127, 0, 0);
			ledsShow();
			delay(500);
		}
		// and throw up our hands to give up
		ESP.reset();
		//ESP.restart();
		}
#endif
		// delay(100);
}
