#include <FS.h>

static File _filesys_upload_file; // a File variable to temporarily store the received file
static int _filesys_upload_size = 0;

static String formatBytes(size_t bytes) { // convert sizes in bytes to KB and MB
	if (bytes < 1024)
		return String(bytes) + "B";
	if (bytes < (1024 * 1024))
		return String(bytes / 1024.0) + "KB";
	if (bytes < (1024 * 1024 * 1024))
		return String(bytes / 1024.0 / 1024.0) + "MB";
	return String(bytes / 1024.0 / 1024.0 / 1024.0) + "GB";
}



bool filesysInit() {	  // Start the SPIFFS and list all contents
	if (SPIFFS.begin()) { // Start the SPI Flash File System (SPIFFS)
		Serial.println("SPIFFS started. Contents:");
		Dir dir = SPIFFS.openDir("/");
		while (dir.next()) { // List the file system contents
			String fileName = dir.fileName();
			size_t fileSize = dir.fileSize();
			Serial.printf("\tFile: %s, size: %s\n", fileName.c_str(), formatBytes(fileSize).c_str());
		}
		Serial.printf("\n");
		return true;
	}
	Serial.println("SPIFFS failed.");
	return false;
}

void filesysLoop() {
	// currently nothing to do
}


bool filesysSaveStart(String name) {
	if (!name.startsWith("/"))
		name = "/" + name;

	if (!name.endsWith(".gz")) {		  // The file server always prefers a compressed version of a file
		String name_as_compressed_gz = name + ".gz"; // So if an uploaded file is not compressed, the existing compressed
		if (SPIFFS.exists(name_as_compressed_gz))	// version of that file must be deleted (if it exists)
			SPIFFS.remove(name_as_compressed_gz);
	}

	_filesys_upload_size = 0;
	Serial.printf("handleFileUpload Name: %s\n", name.c_str());
	_filesys_upload_file = SPIFFS.open(name, "w"); // Open the file for writing in SPIFFS (create if it doesn't exist)

	if (_filesys_upload_file)
		return false;
	return true;
}

bool filesysSaveWrite(uint8_t* buf, size_t size) {
	if (_filesys_upload_file) {
		_filesys_upload_file.write(buf, size); // Write the received bytes to the file
		_filesys_upload_size += size;
	}
	return true; // TODO check return code and/or byte count and return appropriate status
}

bool filesysSaveFinish() {
	if (_filesys_upload_file) {
		_filesys_upload_file.close(); // Close the file again
		Serial.printf("Upload %d bytes\n", _filesys_upload_size);
		_filesys_upload_size = 0;
		return true;
	}
	return false;
}

bool filesysExists(String name) {
	return SPIFFS.exists(name);
}

File filesysOpen(String name) {
	return SPIFFS.open(name, "r"); // Open the file
}

void filesysClose(File handle) {
	handle.close();
}
