# SCREAMZY - the toy kids love and parents hate !

A toy which _was designed to _ respond to volume and pitch to control speed and direction.
The louder you scream, the faster it goes.
A high pitch scream will go right and low pitch scream will go left.

Just imagine the all the fun you will have with a room full of kids!

Unfortunatley, the sound interface is on hold. The PCB has too much signal noise
and the motors inside the platic body makes too much actual noise.

Not to worry, the best feature - head to head racing competion - has been enabled
using a simple multi-touch interface that renders on any smartphone browser.

Each toy will attempt to connect to the local WiFi and if it is unable, then it will
create its own unique hotspot. Multiple units can operate in the same room easily.

![SCREAMZY robot toy](https://gitlab.com/bradanlane/screamzy/raw/master/screamzy.png)

--------------------------------------------------------------------------

--------------------------------------------------------------------------
### Web API
Provides the web server and websockets interfaces.

The ESP8266 creates a web server to serve the interface with is optimized for multi-touch web browsers.
The main interface provides a simulated joystick. The interface is customizable.
For details on the touch interface, see: `http://elder.ninja/locoro/page.php?page=webapp`

The web files are stored in SPIFFS. The files may be uploaded either as an spiffs.bin or
individual files may be uploaded using the web interface: `http://screamzy_xxxxxx.local/upload.html`

The UI is rendered as HTML with JavaScript and sends Json messages through the web socket interface.

The web sockets interface may be extended to add pre-programmed movement 
or even autonomous operation _if additional sensors are added to the robot_.
Add new commands to `webSocketEvent()` withing the section for `WStype_TEXT`.

#### webInit()
Performs all necessary initialization of both the webserver on port 80 and teh websockets interface on port 81.
Must be called once before using any of the other web functions.
 - return: **bool** `true` on success and `false` on failure

#### webLoop()
Give the web interfaces an opportunity to respond to page requests and websockets messages

--------------------------------------------------------------------------
### Motors API
Provides motor speed and direction using either the PCA9685 I2C PWM board or the TB6612FNG motor controller.

The code is closely based off of: https://github.com/Nandopolis/TB6612FNG
and the only changes are to allow for externally controlled standby mode.

The I2C code uses the Adafruit library.

**Useful Notes:**

This code is specific to the ESP8266 and requires int16 for the PWM values.

#### motorsInit()
Performs all necessary initialization. Must be called once before using any of the other motors functions.
 - return: **bool** `true` on success and `false` on failure

#### motorsFullStop()
Stop all motors.
 - return: **nothing**

#### motorsPower()
Drive a single motor at a specificd power/speed. Only useful when there are non-drive motors
 - input:
   - mID **uint16_t** a zero based identification for a motor (with I2C this is 0 .. 15)
   - mSpeed **double** a value between -1 .. +1 for the motor
 - return: **nothing**

#### motorsSpeed()
Drive each motor at a specificd speed - commonly refered to as diferential drive or tank drive mode.
 - input:
   - lSpeed **double** a value between -1 .. +1 for the left motor
   - rSpeed **double** a value between -1 .. +1 for the right motor
 - return: **nothing**

#### motorsDrive()
Drive at a requested speed while turning the requested amount. The actual speed and turn will be constrained.
 - input:
   - speed **double** a value between 0 .. +1
   - turn **double** a value between -1 .. +1 for the amount left (-) or right (+) to turn
   - zero **bool* `true` to inticate a zero radius turn is permitted, `false` will prevent a wheel from rotating backwards.
 - return: **nothing**

--------------------------------------------------------------------------
### LEDS API
Control WS2812 (or SK6812) RGB LEDs

##### Usage:
If you are using an LED matrix, edit the following definitions:
 - `#define LEDS_MATRIX_WIDTH *<value>*`
 - `#define LEDS_MATRIX_HEIGHT *<value>*`

The matrix code assumes the matrix is wired top to bottom and left to right with serpentine wiring
*(the bottom of row 1 is wired to teh bottom of row 2, teh top of row 2 is wired to teh top of row 3, etc.)*

If you are using a string of LEDs, edit the following definition:
 - `#define LEDS_COUNT  *<value>*`

**Notes:**
 - This code currently assumes APA102 type LEDs. To use another type, edit `#define LEDS_TYPE *<value>*`.
 - This code requires `#define PIN_LED_DATA *<value>*` and optionally `#define PIN_LED_CLOCK *<value>*` (if a 2-wire LED type is used)
 - *The SCREAMZY runs the LEDs off of the 3.3V source which may be low for some LEDs and result in erratic behavior*


#### ledsInit()
Performs all necessary initialization. Must be called once before using any of the other LEDS functions.
 - return: **bool** `true` on success and `false` on failure

#### ledsClear()
Clears all LEDs by setting their value to black.
 - return: **bool** `true` on success and `false` on failure

#### ledsShow()
Refreshes the display of the LEDs with their current values. Call this function after changes to the LED values (including `ledsClear()`) to see the results.
 - return: **bool** `true` on success and `false` on failure

#### ledsSetPixelRGB()
Set the value of an LED within an XY matrix to an RGB value.
 - input:
    - pos **uint16_t** a zero based value indicating the index of the LED within the strip
	- red **uint8_t** the red component of the RGB value for the LED
	- green **uint8_t** the green component of the RGB value for the LED
	- blue **uint8_t** the blue component of the RGB value for the LED
 - return: **bool** `true` on success and `false` on failure

#### ledsSetAllRGB()
Set the value of every LED within to an RGB value. and update display
 - input:
	- red **uint8_t** the red component of the RGB value for the LED
	- green **uint8_t** the green component of the RGB value for the LED
	- blue **uint8_t** the blue component of the RGB value for the LED
 - return: **bool** `true` on success and `false` on failure

#### ledsSetPixelIndex()
Set the value of an LED within an XY matrix to an RGB value.
 - input:
    - pos **uint16_t** a zero based value indicating the index of the LED within the strip
	- color **uint8_t** the index into the zero based list of gaming colors: Black, Gold, Cyan, DarkBlue, OrangeRed, DarkMagenta, DarkGreen, DarkRed
 - return: **bool** `true` on success and `false` on failure

#### ledsMatrixSetPixelRGB()
Set the value of an LED within an XY matrix to an RGB value.
 - input:
    - row **uint8_t** a zero based value indicating the X coordinate of the LED within the matrix
    - col **uint8_t** a zero based value indicating the Y coordinate of the LED within the matrix
	- red **uint8_t** the red component of the RGB value for the LED
	- green **uint8_t** the green component of the RGB value for the LED
	- blue **uint8_t** the blue component of the RGB value for the LED
 - return: **bool** `true` on success and `false` on failure

#### ledsMatrixSetPixelIndex()
Set the value of an LED within an XY matrix to an RGB value.
 - input:
    - row **uint8_t** a zero based value indicating the X coordinate of the LED within the matrix
    - col **uint8_t** a zero based value indicating the Y coordinate of the LED within the matrix
	- color **uint8_t** the index into the zero based list of gaming colors: Black, Gold, Cyan, DarkBlue, OrangeRed, DarkMagenta, DarkGreen, DarkRed
 - return: **bool** `true` on success and `false` on failure

--------------------------------------------------------------------------
### WIFI API
Provides wifi or hotspot access and enables over-the-aor updates.

#### wifiInit()
Performs all necessary initialization. Must be called once before using any of the other wifi functions.
 - return: **bool** `true` on success and `false` on failure

#### wifiIsConnected()
get the current availability of wifi connectivity - either via an existing access point or as a hotspot
 - return: **bool** `true` when wifi is available and `false` when not

#### wifiConnectionLost()
test is we still have wifi connectivity; if we never had wifi we can't lose somethign we never had
 - return: **bool** `true` if we lost a prior wifi connection and `false` if we are at the same state as before

#### wifiIsHotspot()
indicate if teh wifi connection is a hotspot or connected to an access point
 - return: **bool** `true` when wifi is a hotspote and `false` when it is an access point

#### wifiAddress()
a conveniece function to get the WiFi address as character string
 - return: **char* ** static internal buffer of the wifi address in the form nnn.nnn.nnn.nnn

#### wifiAddressEnding()
a conveniece function to get the last value in the WiFi address
 - return: **uint8_t** last number in wifi address eg xxx.xxx.xxx.NNN

#### otaInit()
Performs all necessary initialization. Must be called once before using any of the other OAT functions.
 - return: **bool** `true` on success and `false` on failure

#### otaLoop()
Give the OTA an opportunity to respond to over-the-air update requests

--------------------------------------------------------------------------
### ESP8266 GPIO pin assignments

The TB6612FNG motor controller requires 6 output pins (7 if you enable standby mode).
The MAX4466 adjustable gain microphone requries an analog input pin (The ESP8266 only has one).
The WS2812 RGB LEDs require one output pin.

*The ESP8266 has at most 12 usable pins but 2 (or 3) of those have existing useful functions 
which leave 9 (or 10) pins plus the single analog pin for most projects. 
(To get 10 usable pins, the RX pin is repurposed. Most projects do not receive serial data at runtime.)*

